class Elf:    
    def __init__(self, name, level):
        self.name = name
        self.level = level    
    
    def __str__(self):
        return self.name + ";" + str(self.level)