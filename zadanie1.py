# Rozwiń zadnie 1, aby posiadało określone funkcje:
# ===============CENZOR==================

# Dodaj słowo na liste słów zakazanych
# Wyświetl słowa zakazane
# Ocenzuruj tekst

bad_words = [] #stworzenie listy globalnej

def add_bad_words(): #stworzenie funkcji dodajacej brzydkie slowa do listy globalnej
    x = str(input("Podaj brzydkie slowa: ")) #zmienna x zbierajaca brzydkie slowa od użytkownika
    file = open("badwords.txt", "a") #bedziemy dodawac do pliku slowa
    file.write(x + "\n") #to dodaje
    file.close()
    menu() #powrot do menu

def show_bad_words(): #stworzenie funkcji pokaż brzydkie słowa
    try:
        file = open("badwords.txt", "r")
        for line in file:
            print(line.strip()) #pokaż brzydkie slowa
        file.close()
    except(Exception):
        not_found()
    menu() #powrót do menu

def not_found():
    x = int(input("Chcesz utowrzyć plik?\n 1. Tak 2. Nie\n"))
    if (x == 1):
        file = open("badwords.txt", "w")
        file.close()
    elif (x == 2):
        menu()
    else:
        print("Wybrano nieprawidlowa opcje")

def censor(): #stworzenie funkcji cenzurującej
    censored_list = []
    file = open("badwords.txt", "r")
    for word in file:
        censored_list.append(word.strip())
    text = str(input("Podaj tekst do ocenzurowania: ")) #zbieranie od użytkownika tekstu do ocenzuorwania i przypisanie do zmiennej text
    splitted_text = text.split(" ") #rozdzielenie textu po spacjach i stworzenie listy
    output = "" #stworzenie pustej zmiennej string
    for word in splitted_text: #pętla for sprawdzająca jeśli w liście:
        if word in censored_list: #jeśli element z listy jest w brzydkich słowach:
            output += "**** " #dodaj do zmiennej output i ocenzuruj
        else: #pozostałe
            output += word + " " #dodaj do zmiennej output wraz ze spacją
    print(output) #pokaż zmienna output
    file = open("censored_text.txt", "a") #bedziemy dodawac do pliku slowa
    file.write(output + "\n") #to dodaje
    file.close()
    file = open("censored_text.txt", "r")
    for line in file:
        print(line.strip()) #pokaż brzydkie slowa
    file.close()
    menu() #powrot do menu

def censored_text():
    file = open("censored_text.txt", "r")
    for text in file:
        print(text.strip())
    file.close()

def menu(): #stworzenie funkcji menu
    print("""
================CENZOR=================

1. Dodaj słowo na liste słów zakazanych
2. Wyświetl słowa zakazane
3. Ocenzuruj tekst
4. Wyświetl ocenzurowane teksty\n""")
    x = int(input("Wybierz zadanie: ")) #pobranie od użytkownikia decyzji i przypisanie do zmiennej
    if x == 1: #jeśli wybór = 1 to:
        add_bad_words() #uruchom funkcje dodaj brzydkie słowa
    elif x == 2: #jeśli wybór = 2 to:
        show_bad_words() #uruchomn funkcje pokaż brzydkie słowa
    elif x == 3: #jeśli wybór = 3 to:
        censor() #uruchom funkcje cenzurującą
    elif x == 4:
        censored_text()
menu() #uruchom menu