class Gift:    
    def __init__(self, name, level_of_difficulty):
        self.name = name
        self.level_of_difficulty = level_of_difficulty   
    
    def __str__(self):
        return self.name + ";" + str(self.level_of_difficulty)